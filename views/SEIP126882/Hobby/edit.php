<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP126882\Hobby\Hobby;
use App\Bitm\SEIP126882\Utility\Utility;


$hobby= new Hobby();
$singlerow=$hobby->prepare($_GET)->view();


$singleHobbyList=$singlerow['hobbies'];

$singleHobbyArray= explode(",",$singleHobbyList);
//Utility::dd($singleHobbyArray);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>

    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control" id="usr" name="name">
        </div>
        <div class="form-group">
            <label>Last Name:</label>
            <input type="text" class="form-control" id="usr" name="lastname">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding" <?php if(in_array('Coding',$singleHobbyArray)){echo "checked";} else {echo "";}?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cycling"  <?php if(in_array('Cycling',$singleHobbyArray)){echo "checked";}else { echo "";}?>>Cycling</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Swimming" <?php if(in_array('Swimming',$singleHobbyArray)){echo "checked";}else { echo "";}?>>Swimming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array('Playing Football',$singleHobbyArray)){echo "checked";}else { echo "";}?>>Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Watching Movie" <?php if(in_array('Watching Movie',$singleHobbyArray)){echo "checked";}else { echo "";}?>>Watching Movie</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

