<?php
namespace App\Bitm\SEIP126882\ProfilePicture;


use App\Bitm\SEIP126882\Message\Message;
use App\Bitm\SEIP126882\Utility\Utility;

class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;



    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb21") or die("Database connection failed");
    }

    public function prepare($data=array()){
        if(array_key_exists("name",$data)){
            $this->name=  filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("image",$data)){
            $this->image_name=  filter_var($data['image'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;

    }

    public function store(){
        $query="INSERT INTO `atomicprojectb21`.`profilepicture` ( `name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has benn stored successfully");
            Utility::redirect('index.php');

        }else{
            Message::message("Data has not been stored successfully");
            Utility::redirect('index.php');
        }
    }
    public function index(){
        $_allInfo= array();
        $query="SELECT * FROM `profilepicture`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allInfo[]=$row;
        }

        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicprojectb21`.`profilepicture` SET `name` = '{$this->name}', `images` = '{$this->image_name}' WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb21`.`profilepicture` WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }







}